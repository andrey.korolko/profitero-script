require 'curb'
require 'csv'
require 'nokogiri'

categories = []
Nokogiri::HTML(Curl::Easy.perform("https://www.petsonic.com/snacks-huesos-para-perros/").body_str).xpath("//*[@id='subcategories']/ul/li").each do |category|
    categories << category.xpath("h5/a").attribute('href').value
end

if !categories.include? ARGV[0] || ARGV[1] == nil || ARGV[1].empty? 
    puts "Invalid arguments"
    exit
end

i=1
product_urls = []
while((c = Curl::Easy.perform("#{ARGV[0]}?p=#{i}")).response_code == 200) do
    page = Nokogiri::HTML(c.body_str)

    puts "Extracting product links from #{c.url}"
    product_urls << page.xpath("//*[@id='product_list']/li").map do |product|
        product.xpath("div/div/div[2]/div[3]/div[1]/div/a").first.attribute('href').value
    end

    i += 1
end

product_urls.flatten!

puts "\nTotal: #{product_urls.size} \n\n"

CSV.open("#{ARGV[1]}.csv", "w") do |csv|
    i=1
    product_urls.each do |product_url|
        puts "[#{i}..#{product_urls.size}] Downloading #{product_url}"
        c = Curl::Easy.perform(product_url)

        puts "Parsing #{product_url}"
        page = Nokogiri::HTML(c.body_str)

        puts "Extracting name from #{product_url}"
        name = page.xpath("//*[@id='center_column']/div/div/div[2]//h1").text.match(/([A-Za-z0-9\+\-]+\s)+/)[0].chomp

        puts "Extracting image from #{product_url}"
        img = page.xpath("//*[@id='bigpic']").attribute('src').value

        items = page.xpath("//*[@id='attributes']/fieldset/div/ul/li")
        puts "Product option count: #{items.size}"
        k=1
        items.each do |item|
            puts "Extract [#{k}] option..."
            option = item.xpath("label/span[1]").text

            puts "Extracting price of [#{k}] option from #{product_url}"
            price = item.xpath("label/span[2]").text.match(/[0-9]+\.[0-9]+/)[0]

            puts "Write record into file..."
            csv << [name + ' - ' + option, price, img]

            k += 1
        end

        i += 1
    end
end